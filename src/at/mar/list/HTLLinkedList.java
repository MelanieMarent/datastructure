package at.mar.list;

public class HTLLinkedList {
	private Node root;
	
	public void add(int  value){
		if(root == null){
			Node n = new Node(value, null);
			this.root = n;
		}
		else{
			Node actNode = root;
			while(actNode.next != null){
				actNode = actNode.next;
			}
			Node n = new Node(value,null);
			actNode.next = n;
		}
	}
	
	public void clear(){
		root = null;
	}
	
	public void remove(int index){
		Node actNode = root;
		int i = 0;
		while(i < index-1){
			actNode = actNode.next;
			i++;
		}
		actNode.next = actNode.next.next;
	}
	
	public int get(int index){
		Node actNode = root;
		int i = 0;
		while(i < index-1){
			actNode = actNode.next;
			i++;
		}
		return actNode.value;
	}
}
